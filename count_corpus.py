from dev.corpus_extractor.corpus_extraction_paths import CorpusExtractionPaths
from dev.corpus_extractor.extractor import Extractor
import numpy as np
from collections import defaultdict, OrderedDict


class ComponentCounts:
    def __init__(self, context_name, component_type, component):
        self._context_name = context_name
        self._component_type = component_type
        self._component = component

    def _extract_all_references(self, reference_component, include_multiple_refs=False, select_first_target_vis=True):
        references = []
        for reference_idx, reference in enumerate(reference_component):
            data_pair = list(zip(*[(target_vis_id, plot_type) for target_vis_id, plot_type in
                                   zip(reference.get_targetvis_ids_attribute().split(','),
                                       reference.get_targetvis_plottypes_attribute().split(',')) if
                                   ('*' not in target_vis_id or include_multiple_refs)]))
            if not data_pair:
                continue

            target_vis_ids, plot_types = data_pair
            if not target_vis_ids:
                continue
            target_vis_ids = sorted(target_vis_ids)
            target_vis_id = target_vis_ids[0] if select_first_target_vis else target_vis_ids
            plot_type = plot_types[0] if select_first_target_vis else plot_types

            source_vis_id = -1
            if reference.get_sourcevis_ids_attribute():
                source_vis_ids = sorted(
                    [v.strip() for v in reference.get_sourcevis_ids_attribute().split(',') if ('*' not in v
                     or include_multiple_refs)])

                if source_vis_ids:
                    source_vis_id = source_vis_ids[0]

            references.append(
                (reference_idx, source_vis_id, target_vis_id, plot_type, reference.get_properties_attribute(),
                 reference.get_referringexpression_attribute(), reference.get_utteranceid_attribute(),
                 reference.get_timestep_attribute())
            )

        return references

    def _extract_reference(self, reference_component, which_one):
        references = self._extract_all_references(reference_component=reference_component)

        if not references:
            return -1, -1, -1, None, None, 'none', -1, None

        if which_one != -1:
            ref_idx, source_vis_id, target_vis_id, plot_type, ref_prop, ref_text, ref_uttid, timestep = \
                references[which_one]
            return ref_idx, source_vis_id, target_vis_id, plot_type, ref_prop, ref_text, ref_uttid, timestep

    def _is_create_vis_request(self, visrefexps, dialogue_state):
        ref_idx, _, target_vis_id, _, _, _, ref_uttid, timestep = self._extract_reference(
            reference_component=visrefexps, which_one=0)

        if timestep != 'current':
            return False

        if target_vis_id in dialogue_state:
            return False

        if visrefexps[ref_idx].get_sourcevis_ids_attribute() == 'none' and \
                visrefexps[ref_idx].get_targetvis_ids_attribute() != 'none' and \
                target_vis_id not in dialogue_state:
            return True

        return False

    def _is_modify_vis_request(self, visrefexps, dialogue_state):
        ref_idx, _, target_vis_id, _, _, _, ref_uttid, timestep = self._extract_reference(
            reference_component=visrefexps, which_one=0)

        if timestep != 'current':
            return False

        if target_vis_id in dialogue_state:
            return False

        if visrefexps[ref_idx].get_sourcevis_ids_attribute() == \
                visrefexps[ref_idx].get_targetvis_ids_attribute():
            return False

        if visrefexps[ref_idx].get_sourcevis_ids_attribute() != 'none' and \
                visrefexps[ref_idx].get_targetvis_ids_attribute() != 'none':
            return True

        return False

    def _is_existing_vis_request(self, visrefexps, dialogue_state):
        ref_idx, _, target_vis_id, _, _, _, ref_uttid, timestep = self._extract_reference(
            reference_component=visrefexps, which_one=0)

        if timestep != 'current':
            return False

        if target_vis_id not in dialogue_state:
            return False

        if visrefexps[ref_idx].get_sourcevis_ids_attribute() == 'none':
            return False

        if visrefexps[ref_idx].get_sourcevis_ids_attribute() == \
                visrefexps[ref_idx].get_targetvis_ids_attribute():
            return True

        return False

    def _is_non_request(self, visrefexps, dialogue_state):
        ref_idx, _, target_vis_id, _, _, _, ref_uttid, timestep = self._extract_reference(
            reference_component=visrefexps, which_one=0)

        if timestep != 'current':
            return False

        return True

    def _update_dialogue_history(self, dialogue_state):
        def update_dialogue_state(visrefexps, dialogue_state):
            for ref_idx, source_vis_id, target_vis_id, plot_type, ref_prop, ref_text, ref_uttid, timestep in \
                    self._extract_all_references(reference_component=visrefexps):

                if timestep != 'current':
                    continue

                if source_vis_id == target_vis_id:
                    continue

                elif source_vis_id != 'none' and target_vis_id != 'none:':
                    dialogue_state[target_vis_id] = target_vis_id

                elif source_vis_id == 'none' and target_vis_id != 'none':
                    dialogue_state[target_vis_id] = target_vis_id

        referring_expressions = [[(utterance, vis_ref) for utterance, gesture, gesture_refexp,
                                                           text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        if len(referring_expressions) == 0:
            return

        for utterance, visrefexps in referring_expressions[0]:
            if visrefexps is None:
                continue

            update_dialogue_state(visrefexps, dialogue_state)

    def _get_total_gestures(self):
        return int(
            np.sum(
                np.sum(
                    [
                        [len(gesture) for utterance, gesture, gesture_refexp, text_refexp, vis_ref in
                         [comp.get_context_component()] if gesture is not None] for comp in self._component
                    ]
                )
            )
        )

    def _get_total_gesture_features(self):
        gesture_features_count = defaultdict(int)
        for comp in self._component:
            for utterance, gestures, gesture_refexp, text_refexp, vis_ref in [comp.get_context_component()]:
                if not gestures:
                    continue

                for gesture in gestures:
                    if gesture.get_mode_attribute():
                        gesture_features_count[gesture.get_mode_attribute()] += 1
                    if gesture.get_target_attribute():
                        gesture_features_count[gesture.get_target_attribute()] += 1
                    if gesture.get_type_attribute():
                        gesture_features_count[gesture.get_type_attribute()] += 1
                    if gesture.get_space_attribute():
                        gesture_features_count[gesture.get_space_attribute()] += 1
                    if gesture.get_timestep_attribute():
                        gesture_features_count[gesture.get_timestep_attribute()] += 1
        return gesture_features_count

    def _get_total_gesture_references(self, dialogue_state):
        def is_gesture_cooccurring_with_text_reference(refexps, dialogue_state):
            _, _, target_vis_id, _, _, ref_text, ref_uttid, _ = \
                self._extract_reference(which_one=0, reference_component=refexps)

            if ref_text != 'none' and target_vis_id in dialogue_state:
                return True

            return False

        referring_expressions = [[(utterance, gesture, gesture_refexp, vis_ref) for utterance, gesture, gesture_refexp,
                                                                           text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        if len(referring_expressions) == 0:
            return 0

        total_references = np.sum([
            1 if refexps and
                 visrefexps and
                 (self._is_modify_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state) or
                  self._is_existing_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state)) and
                 is_gesture_cooccurring_with_text_reference(refexps=refexps, dialogue_state=dialogue_state)
            else 0
            for utterance, gesture, refexps, visrefexps in referring_expressions[0]
        ])

        return total_references

    def _get_total_cntxt_gesture_references(self, dialogue_state):
        gesture_features_count = defaultdict(int)

        def gesture_cooccurring_with_text_reference_cnt(refexps, dialogue_state):
            cnt = 0
            if not refexps:
                return cnt

            for _, _, target_vis_id, _, _, ref_text, ref_uttid, _ in \
                self._extract_all_references(reference_component=refexps, include_multiple_refs=True):

                if ref_text != 'none':
                    cnt += 1

            return cnt

        referring_expressions = [[(utterance, gesture, gesture_refexp, vis_ref) for utterance, gesture, gesture_refexp,
                                                                           text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        if len(referring_expressions) == 0:
            return 0, gesture_features_count

        total_references = np.sum([
            gesture_cooccurring_with_text_reference_cnt(refexps=refexps, dialogue_state=dialogue_state)
            for utterance, gestures, refexps, visrefexps in referring_expressions[0]
        ])

        if total_references > 0:
            for utterance, gestures, refexps, _ in referring_expressions[0]:
                if not gestures:
                    continue

                for refexp in refexps:
                    gesture_id = refexp.get_gestureid_attribute()

                    for gesture in gestures:
                        if gesture.get_gestureid_attribute() != gesture_id:
                            continue
                        if gesture.get_mode_attribute():
                            gesture_features_count[gesture.get_mode_attribute()] += 1
                        if gesture.get_target_attribute():
                            gesture_features_count[gesture.get_target_attribute()] += 1
                        if gesture.get_type_attribute():
                            gesture_features_count[gesture.get_type_attribute()] += 1
                        if gesture.get_space_attribute():
                            gesture_features_count[gesture.get_space_attribute()] += 1
                        if gesture.get_timestep_attribute():
                            gesture_features_count[gesture.get_timestep_attribute()] += 1

        return total_references, gesture_features_count

    def _get_total_text_references(self, dialogue_state):
        def is_text_reference(refexps, dialogue_state):
            _, _, target_vis_id, _, _, ref_text, ref_uttid, _ = \
                self._extract_reference(which_one=0, reference_component=refexps)

            if ref_text != 'none' and target_vis_id in dialogue_state:
                return True

            return False

        referring_expressions = [[(utterance, text_refexp, vis_ref) for utterance, gesture, gesture_refexp,
                                                                        text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        if len(referring_expressions) == 0:
            return 0

        total_references = np.sum([
            1 if refexps and
                 visrefexps and
                 (self._is_modify_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state) or
                  self._is_existing_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state)) and
                 is_text_reference(refexps=refexps, dialogue_state=dialogue_state)
            else 0
            for utterance, refexps, visrefexps in referring_expressions[0]
        ])

        return total_references

    def _get_total_cntxt_text_references(self, dialogue_state):
        def text_reference_cnt(refexps, dialogue_state):
            cnt = 0
            if not refexps:
                return cnt

            for _, _, target_vis_id, _, _, ref_text, ref_uttid, _ in \
                self._extract_all_references(reference_component=refexps, include_multiple_refs=True):

                if ref_text != 'none':
                    cnt += 1

            return cnt

        referring_expressions = [[(utterance, text_refexp, vis_ref) for utterance, gesture, gesture_refexp,
                                                                           text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        if len(referring_expressions) == 0:
            return 0

        total_references = np.sum([
            text_reference_cnt(refexps=refexps, dialogue_state=dialogue_state)
            for utterance, refexps, visrefexps in referring_expressions[0]
        ])

        return total_references

    def _get_total_established_references(self, dialogue_state):
        def established_reference_cnt(refexps):
            _, _, target_vis_id, _, ref_prop, ref_text, ref_uttid, _ = \
                self._extract_reference(which_one=0, reference_component=refexps)

            if not ref_prop:
                return False

            if ref_prop == 'none':
                return False

            prop = [property.replace('_', '').lower() for property in
                               sorted(ref_prop.replace('[', '').replace(']', '').split(';'))]
            prop = [property if '@@@' not in property else property.split('@@@')[0]
                                                for property in prop]

            return len(prop)

        referring_expressions = [[(utterance, vis_ref) for utterance, gesture, gesture_refexp,
                                                                        text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        if len(referring_expressions) == 0:
            return 0

        total_references = np.sum([
            established_reference_cnt(refexps=visrefexps) if visrefexps and
                 (self._is_create_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state) or
                  self._is_modify_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state))
            else 0
            for utterance, visrefexps in referring_expressions[0]
        ])

        return total_references

    def _get_total_follow_up_query_established_references(self, dialogue_state):
        def established_reference_cnt(refexps):
            _, _, target_vis_id, _, ref_prop, ref_text, ref_uttid, _ = \
                self._extract_reference(which_one=0, reference_component=refexps)

            if not ref_prop:
                return False

            if ref_prop == 'none':
                return False

            prop = [property.replace('_', '').lower() for property in
                               sorted(ref_prop.replace('[', '').replace(']', '').split(';'))]
            prop = [property.split('@@@')[0] for property in prop if '@@@' in property]

            return len(prop)

        referring_expressions = [[(utterance, vis_ref) for utterance, gesture, gesture_refexp,
                                                                        text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        if len(referring_expressions) == 0:
            return 0

        total_references = np.sum([
            established_reference_cnt(refexps=visrefexps) if visrefexps and
                 (self._is_create_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state) or
                  self._is_modify_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state))
            else 0
            for utterance, visrefexps in referring_expressions[0]
        ])

        return total_references

    def _get_gesture_stats(self, dialogue_state):
        def is_gesture_cooccurring_with_text_reference(refexps, dialogue_state):
            if not refexps:
                return False

            for _, _, target_vis_id, _, _, ref_text, ref_uttid, _ in \
                self._extract_all_references(reference_component=refexps, include_multiple_refs=True, select_first_target_vis=False):

                if ref_text != 'none' and '*' not in target_vis_id:
                    return True

            return False

        referring_expressions = [[(utterance, gesture, gesture_refexp, vis_ref) for utterance, gesture, gesture_refexp,
                                                                           text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        modify_vis_cnt = 0
        create_vis_cnt = 0
        existing_vis_cnt = 0
        other_cnt = 0
        gesture_ref_cnt = 0

        if len(referring_expressions) == 0:
            return create_vis_cnt, modify_vis_cnt, existing_vis_cnt, other_cnt, gesture_ref_cnt

        for referring_expression in [referring_expressions[0]]:

            for utterance, gestures, gesture_refexps, visrefexps in referring_expression:
                if not is_gesture_cooccurring_with_text_reference(refexps=gesture_refexps, dialogue_state=dialogue_state):
                    continue

                gesture_ref_cnt += 1

                if not visrefexps:
                    other_cnt += 1
                    continue

                if self._is_modify_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state):
                    modify_vis_cnt += 1

                elif self._is_existing_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state):
                    existing_vis_cnt += 1

                elif self._is_create_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state):
                    create_vis_cnt += 1

                else:
                    other_cnt += 1

        return create_vis_cnt, modify_vis_cnt, existing_vis_cnt, other_cnt, gesture_ref_cnt

    def _get_text_stats(self, dialogue_state):
        def is_text_reference(refexps, dialogue_state):
            if not refexps:
                return False

            for _, _, target_vis_id, _, _, ref_text, ref_uttid, _ in \
                self._extract_all_references(reference_component=refexps, include_multiple_refs=True, select_first_target_vis=False):

                if ref_text != 'none' and '*' not in target_vis_id:
                    return True

            return False

        referring_expressions = [[(utterance, text_refexp, vis_ref) for utterance, gesture, gesture_refexp,
                                                                           text_refexp, vis_ref in
                                  [comp.get_context_component()]] for comp in self._component]

        modify_vis_cnt = 0
        create_vis_cnt = 0
        existing_vis_cnt = 0
        other_cnt = 0
        text_ref_cnt = 0

        if len(referring_expressions) == 0:
            return create_vis_cnt, modify_vis_cnt, existing_vis_cnt, other_cnt, text_ref_cnt

        for referring_expression in [referring_expressions[0]]:

            for utterance, text_refexps, visrefexps in referring_expression:
                if not is_text_reference(refexps=text_refexps, dialogue_state=dialogue_state):
                    continue

                text_ref_cnt += 1

                if not visrefexps:
                    other_cnt += 1
                    continue

                if self._is_modify_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state):
                    modify_vis_cnt += 1

                elif self._is_existing_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state):
                    existing_vis_cnt += 1

                elif self._is_create_vis_request(visrefexps=visrefexps, dialogue_state=dialogue_state):
                    create_vis_cnt += 1

                else:
                    other_cnt += 1

        return create_vis_cnt, modify_vis_cnt, existing_vis_cnt, other_cnt, text_ref_cnt

    def count_component(self, counts: dict, dialogue_state: dict):
        context_name, component_type, component = self._context_name, self._component_type, self._component

        counts['utterances'] += len(component)
        counts[component_type] += len(component)
        counts[context_name] += 1 if len(component) > 0 else 0
        counts[context_name + '_utterances'] += 1 if len(component) > 0 else 0
        counts[component_type + '_utterances'] += len(component)

        total_gestures = self._get_total_gestures()
        counts['gestures'] += total_gestures
        counts[component_type + '_gestures'] += total_gestures
        counts[context_name + '_gestures'] += 1 if total_gestures > 0 else 0
        gesture_features_count = self._get_total_gesture_features()
        if 'gesture_features_count' not in counts:
            counts['gesture_features_count'] = defaultdict(int)
        for name, cnt in gesture_features_count.items():
            counts['gesture_features_count'][name] += cnt

        total_references = self._get_total_established_references(dialogue_state=dialogue_state)
        counts['established_references'] += total_references
        counts['references'] += total_references
        counts['cntxt_references'] += 1 if total_references > 0 else 0
        counts[component_type + '_established_references'] += total_references
        counts[context_name + '_established_references'] += 1 if total_references > 0 else 0

        total_references = \
            self._get_total_follow_up_query_established_references(dialogue_state=dialogue_state)
        counts['follow_up_established_references'] += total_references
        counts[component_type + '_follow_up_established_references'] += total_references
        counts[context_name + '_follow_up_established_references'] += 1 if total_references > 0 else 0

        total_references = self._get_total_gesture_references(dialogue_state=dialogue_state)
        counts['gesture_references'] += total_references
        counts['references'] += total_references
        counts[component_type + '_gesture_references'] += total_references
        counts[context_name + '_gesture_references'] += 1 if total_references > 0 else 0

        total_references, gesture_features_count = \
            self._get_total_cntxt_gesture_references(dialogue_state=dialogue_state)
        counts['cntxt_references'] += 1 if total_references > 0 else 0
        counts['cntxt_gesture_references'] += total_references
        if 'cntxt_gesture_references_features' not in counts:
            counts['cntxt_gesture_references_features'] = defaultdict(int)
        for name, cnt in gesture_features_count.items():
            counts['cntxt_gesture_references_features'][name] += cnt
        counts[component_type + '_cntxt_gesture_references'] += total_references
        counts[context_name + '_cntxt_gesture_references'] += 1 if total_references > 0 else 0

        create_vis_cnt, modify_vis_cnt, existing_vis_cnt, other_cnt, gesture_ref_cnt = self._get_gesture_stats(dialogue_state=dialogue_state)
        counts['gesture_ref_create_vis_cnt'] += create_vis_cnt
        counts[component_type + '_gesture_ref_create_vis_cnt'] += create_vis_cnt
        counts[context_name + '_gesture_ref_create_vis_cnt'] += 1 if create_vis_cnt > 0 else 0

        counts["gesture_ref_modify_vis_cnt"] += modify_vis_cnt
        counts[component_type + '_gesture_ref_modify_vis_cnt'] += modify_vis_cnt
        counts[context_name + '_gesture_ref_modify_vis_cnt'] += 1 if modify_vis_cnt > 0 else 0

        counts['gesture_ref_existing_vis_cnt'] += existing_vis_cnt
        counts[component_type + '_gesture_ref_existing_vis_cnt'] += existing_vis_cnt
        counts[context_name + '_gesture_ref_existing_vis_cnt'] += 1 if existing_vis_cnt > 0 else 0

        counts['gesture_ref_other_cnt'] += other_cnt
        counts[component_type + '_gesture_ref_other_cnt'] += other_cnt
        counts[context_name + '_gesture_ref_other_cnt'] += 1 if other_cnt > 0 else 0

        counts['gesture_ref_cnt'] += gesture_ref_cnt
        counts[component_type + '_gesture_ref_cnt'] += gesture_ref_cnt
        counts[context_name + '_gesture_ref_cnt'] += 1 if gesture_ref_cnt > 0 else 0

        create_vis_cnt, modify_vis_cnt, existing_vis_cnt, other_cnt, text_ref_cnt = self._get_text_stats(dialogue_state=dialogue_state)
        counts['text_ref_create_vis_cnt'] += create_vis_cnt
        counts[component_type + '_text_ref_create_vis_cnt'] += create_vis_cnt
        counts[context_name + '_text_ref_create_vis_cnt'] += 1 if create_vis_cnt > 0 else 0

        counts["text_ref_modify_vis_cnt"] += modify_vis_cnt
        counts[component_type + '_text_ref_modify_vis_cnt'] += modify_vis_cnt
        counts[context_name + '_text_ref_modify_vis_cnt'] += 1 if modify_vis_cnt > 0 else 0

        counts['text_ref_existing_vis_cnt'] += existing_vis_cnt
        counts[component_type + '_text_ref_existing_vis_cnt'] += existing_vis_cnt
        counts[context_name + '_text_ref_existing_vis_cnt'] += 1 if existing_vis_cnt > 0 else 0

        counts['text_ref_other_cnt'] += other_cnt
        counts[component_type + '_text_ref_other_cnt'] += other_cnt
        counts[context_name + '_text_ref_other_cnt'] += 1 if other_cnt > 0 else 0

        counts['text_ref_cnt'] += text_ref_cnt
        counts[component_type + '_text_ref_cnt'] += text_ref_cnt
        counts[context_name + '_text_ref_cnt'] += 1 if text_ref_cnt > 0 else 0

        total_references = self._get_total_text_references(dialogue_state=dialogue_state)
        counts['text_references'] += total_references
        counts['references'] += total_references
        counts[component_type + '_text_references'] += total_references
        counts[context_name + '_text_references'] += 1 if total_references > 0 else 0

        total_references = self._get_total_cntxt_text_references(dialogue_state=dialogue_state)
        counts['cntxt_references'] += 1 if total_references > 0 else 0
        counts['cntxt_text_references'] += total_references
        counts[component_type + '_cntxt_text_references'] += total_references
        counts[context_name + '_cntxt_text_references'] += 1 if total_references > 0 else 0

        self._update_dialogue_history(dialogue_state=dialogue_state)


counts = defaultdict(int)
data = Extractor.extract(
    corpus_path=CorpusExtractionPaths.JSON_CORPUS_DATA, utterance_cutoff=-1, process_refexps=False)
for subject in data:
    subject_name = subject[0]
    contexts = subject[1]

    print("Subject", subject_name)

    dialogue_state = defaultdict(str)

    for context in contexts:
        context_names = ['setup', 'request', 'conclusion']
        component_types = ['prev', 'current', 'next']
        components = [context.get_setup(), [context.get_request()], context.get_conclusion()]

        for context_name, component_type, component in \
                zip(context_names, component_types, components):
            ComponentCounts(context_name=context_name, component_type=component_type, component=component). \
                count_component(counts=counts, dialogue_state=dialogue_state)

for name, cnt in counts.items():
    if type(cnt) == defaultdict:
        print(name)
        for k, v in cnt.items():
            print('\t' + k + '\t' + str(v))
    else:
        print(name, cnt)
